# STEPS TO FOLLOW:

### 1 - Create new project

- Use express generator to create a new project with view as hbs `express --view=hbs backend-guide`
- Add .gitignore to exclude /node_modules and .env.
- Push to your git repository **master** branch.

### 2 - Branch develop and prepare project configuration

- Checkout to develop and `git push origin -u develop`. From now on, we'll work from this branch only (no on this branch, but from it). After the `origin -u branch-name` part is done once, you will only need to `git push` in that branch.

- Check out to a new branch `feature/initial-config` and install `nodemon` as dev dependency `npm i --save-dev nodemon`, create a new script in `package.json` such as this: `dev: "nodemon www/bin"`.

- Add this to the script so we can use debug and see messages correctly formatted in our terminal: `"dev": "DEBUG=backend-guide:* nodemon ./bin/www"`. Anytime we use `debug('This is a test message)` we'll see it in our terminal logs.

- Create a `.prettierrc` file in the root of your project, install the package in VSCode and add the next configuration:

```
  {
    "tabWidth": 2,
    "semi": false,
    "trailingComma": "none",
    "singleQuote": true,
    "printWidth": 120
  }
```

Anytime you format your document, it will look clean and nice.

- Install eslint as dev dependecy `npm i eslint --save-dev`. Run `npx eslint --init` in your terminal and follow the steps, approve the installation of dependencies and enjoy your clean code! (Remember to use Module.js and Standar library for node).

- Install **dotenv** to use environment variables in the project. Require the library in the start of `app.js` to avoid errors and have them available anywhere as `require('dotenv').config()`. If you create a `.env` file, it will read the variables and show them in `process.env.VARIABLE_NAME`.

### 3 - Start creating endpoints to test our API

- Simulation of a promise via then/catch or async/await (+try/catch):

```
  const books = ['Titanic', 'It', 'Animal Farm', 'The Witcher', 'Harry Potter']

  // Simulate a promise request to our DB that can either resolve or reject
  function getBooks() {
    return Promise.resolve(books);
    // return Promise.reject(new Error('There was an error getting the books'));
  }

  getBooks()
    .then(booksRes => {
      console.log('The books', booksRes)
    })
    .catch(err => {
      console.log(err.message)
    })

  async function fetchBooks() {
    try {
      const books = await getBooks()
      console.log('The async books', books)
    } catch (e) {
      console.log('The async error', e.message)
    }
  }

  fetchBooks()
```

### 4 - After creating a few endpoints, lets add a MongoDB system

- Install **mongoose**, it's a ODM library to handle our schemas and data easily.
- Create a **config** folder and a **db.js** file inside where we'll keep our database startup config.

```
  const debug = require('debug')('backend-guide:db')
  const mongoose = require('mongoose')

  const DB_URI = process.env.DB_URI || 'mongodb://localhost:27017/backend-guide'

  mongoose
    .connect(DB_URI, { useNewUrlParser: true })
    .then(() => {
      debug(`Connected to the database: ${DB_URI}`)
    })
    .catch(() => {
      debug('There was an error connecting to the database!')
    })
```

- Create a seeds file to populate our database after creating the first model.

- Remove the /:name params and addapt the project to the new books model and behaviour.

### Additional notes:

- When in `develop`, create your own branch before working in a new feature by doing: `git checkout -b feature/branch-name`.

- When you are finished with the new feature, push to github after commiting changes by doing `git push origin -u feature/branch-name` and open a **Pull Request** in **GitHub** poiting to **develop**.

- When your teammates approve the feature and changes, do the merge via **GitHub** and you'll have the code available in `develop`. Just do a checkout to develop and `git pull` to bring the new content.

- Repeat the process to create new features!
