const mongoose = require('mongoose')

// const Schema = mongoose.Schema and const model = mongoose.model can de declared with destructuring:
const { Schema, model } = mongoose

const reviewSchema = new Schema(
  {
    content: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
      minlength: 20
    }
  },
  {
    toJSON: {
      transform: (doc, ret) => {
        ret.id = doc._id

        delete ret._id
        delete ret.createdAt
        delete ret.updatedAt
        delete ret.__v

        return ret
      }
    },
    timestamps: true
  }
)

const Review = model('Review', reviewSchema)
module.exports = Review
