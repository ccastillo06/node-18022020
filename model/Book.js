const mongoose = require('mongoose')

// const Schema = mongoose.Schema and const model = mongoose.model can de declared with destructuring:
const { Schema, model } = mongoose

const bookSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      lowercase: true,
      trim: true
    },
    year: {
      type: Number,
      required: true
    },
    reviews: [{ type: mongoose.Types.ObjectId, ref: 'Review' }] // Array of mongo documents ids
  },
  {
    toJSON: {
      // doc is the document in the db, ret is the object transformation to json
      // This will let us configure the json obtained as a response
      transform: (doc, ret) => {
        ret.id = doc._id

        delete ret._id
        delete ret.createdAt
        delete ret.updatedAt
        delete ret.__v

        return ret
      }
    },
    timestamps: true
  }
)

// A model is ALWAYS in uppercase and singular, mongoose will name the collection with lowercase and plural
const Book = model('Book', bookSchema) // Will be the books collection
module.exports = Book
