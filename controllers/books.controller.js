const createError = require('http-errors')

const Book = require('../model/Book')

const getBooks = (req, res, next) => {
  Book.find()
    .then(books => {
      res.status(200).render('books', { books: books })
    })
    .catch(err => {
      next(err)
    })
}

// We will receive the book id in req.params.id
const getBookById = (req, res, next) => {
  const id = req.params.id

  Book.findById(id)
    .populate('reviews')
    .then(book => {
      res.status(200).render('book', { book: book })
    })
    .catch(() => {
      next(createError(404))
    })
}

const createBook = (req, res, next) => {
  const newBook = new Book({
    name: req.body.name,
    year: req.body.year
  })

  newBook
    .save()
    .then(() => {
      res.redirect('/books')
    })
    .catch(err => {
      next(err)
    })
}

const editBook = (req, res, next) => {
  const id = req.body.id
  const name = req.body.name
  const year = req.body.year

  Book.findByIdAndUpdate(id, { name, year })
    .then(() => {
      res.redirect('/books')
    })
    .catch(err => {
      next(err)
    })
}

const deleteBook = (req, res, next) => {
  const id = req.params.id

  Book.findByIdAndDelete(id)
    .then(() => {
      res.status(200).json('Deleted book!')
    })
    .catch(err => {
      next(err)
    })
}

// These controllers are related to other models
const addReview = (req, res, next) => {
  const bookId = req.body.bookId
  const reviewId = req.body.reviewId

  // We use $addToSet instead of $push in order to prevent duplicates
  Book.findByIdAndUpdate(bookId, { $addToSet: { reviews: reviewId } })
    .then(() => {
      res.status(200).json('Updated correcly!')
    })
    .catch(err => {
      next(err)
    })
}

const deleteReview = (req, res, next) => {
  const bookId = req.body.bookId
  const reviewId = req.body.reviewId

  Book.findByIdAndUpdate(bookId, { $pull: { reviews: reviewId } })
    .then(() => {
      res.status(200).json('Deleted review from book correcly!')
    })
    .catch(err => {
      next(err)
    })
}

module.exports = {
  getBooks,
  getBookById,
  createBook,
  editBook,
  deleteBook,
  addReview,
  deleteReview
}
