// const createError = require('http-errors')

const Review = require('../model/review')

const getReviews = (req, res, next) => {
  Review.find()
    .then(reviews => {
      res.status(200).json(reviews)
    })
    .catch(err => {
      next(err)
    })
}

const createReview = (req, res, next) => {
  const newReview = new Review({
    content: req.body.content
  })

  newReview
    .save()
    .then(createdReview => {
      res.status(200).json(createdReview)
    })
    .catch(err => {
      next(err)
    })
}

module.exports = {
  getReviews,
  createReview
}
