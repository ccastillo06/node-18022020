// const debug = require('debug')('backend-guide:books.route')
const express = require('express')

const booksController = require('../controllers/books.controller')

const router = express.Router()

router.get('/', booksController.getBooks)
// This route will assign the value in /:id to a variable name.
// Example: /books/Titanic => { id: 'ua129hf8anb3' }
router.get('/:id', booksController.getBookById)

router.post('/new', booksController.createBook)
router.post('/edit', booksController.editBook)

router.delete('/:id', booksController.deleteBook)

// Other model related endpoints
router.post('/add-review', booksController.addReview)
router.post('/delete-review', booksController.deleteReview)

module.exports = router
