// const debug = require('debug')('backend-guide:books.route')
const express = require('express')

const reviewController = require('../controllers/review.controller')

const router = express.Router()

router.get('/', reviewController.getReviews)

router.post('/new', reviewController.createReview)

module.exports = router
