const mongoose = require('mongoose')

const Book = require('../model/Book')

const books = [
  {
    name: 'Titanic',
    year: 1919
  },
  {
    name: 'It',
    year: 1920
  },
  {
    name: 'Animal Farm',
    year: 1850
  },
  {
    name: 'The Witcher',
    year: 2004
  },
  {
    name: 'Harry Potter',
    year: 2005
  }
]

const DB_URI = process.env.DB_URI || 'mongodb://localhost:27017/backend-guide'

mongoose
  .connect(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log(`Connected to the database: ${DB_URI}`)
  })
  .then(async () => {
    await Book.deleteMany()
    console.log('Delete Book collection')

    for (const book of books) {
      const newBook = new Book(book)

      await Book.create(newBook)
    }

    console.log('Finished creating books')
  })
  .catch(() => {
    console.log('There was an error connecting to the database!')
  })
  .finally(() => mongoose.disconnect())
